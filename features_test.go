// GEOPACKAGEGO Test Suite by Roma Hicks is marked with CC0 1.0 Universal.
// To view a copy of this license, visit http://creativecommons.org/publicdomain/zero/1.0

package geopackage

import (
	"testing"
)

func TestParseHeader(t *testing.T) {
	t.Run("Parse geometry headers, little Endian.", func(t *testing.T) {
		r := &Row{}
		r.Data = append(r.Data, []byte{0, 0, 0, 127, 228, 115, 0, 0})
		r.parseHeader(0)
		if r.SRSID != 29668 {
			t.Errorf("Expected SRID of 29668 but recieved: %v\n", r.SRSID)
		}
	})
	t.Run("Parse geometry headers, big Endian.", func(t *testing.T) {
		r := &Row{}
		r.Data = append(r.Data, []byte{0, 0, 0, 0, 0, 0, 115, 228})
		r.parseHeader(0)
		if r.SRSID != 29668 {
			t.Errorf("Expected SRID of 29668 but recieved: %v\n", r.SRSID)
		}
	})
}

func TestParseFlags(t *testing.T) {
	t.Run("Parse full byte.", func(t *testing.T) {
		var b byte = 255
		target := []byte{1, 1, 1, 1, 1, 1, 1, 1}
		r := &Row{}
		rb := r.parseFlags(b)
		for i, v := range rb {
			if v != target[i] {
				t.Errorf("%v\n", rb)
				t.FailNow()
			}
		}
	})
	t.Run("Parse empty byte.", func(t *testing.T) {
		var b byte
		target := []byte{0, 0, 0, 0, 0, 0, 0, 0}
		r := &Row{}
		rb := r.parseFlags(b)
		for i, v := range rb {
			if v != target[i] {
				t.Errorf("%v\n", rb)
				t.FailNow()
			}
		}
	})
	t.Run("Parse a byte with a value of 54.", func(t *testing.T) {
		var b byte = 54
		target := []byte{0, 0, 1, 1, 0, 1, 1, 0}
		r := &Row{}
		rb := r.parseFlags(b)
		for i, v := range rb {
			if v != target[i] {
				t.Errorf("%v\n", rb)
				t.FailNow()
			}
		}
	})
	t.Run("Parse a byte with a value of 1.", func(t *testing.T) {
		var b byte = 1
		target := []byte{0, 0, 0, 0, 0, 0, 0, 1}
		r := &Row{}
		rb := r.parseFlags(b)
		for i, v := range rb {
			if v != target[i] {
				t.Errorf("%v\n", rb)
				t.FailNow()
			}
		}
	})
}

func TestReworkGeometry(t *testing.T) {
	t.Run("Strip minimal GeoPackage Binary Header.", func(t *testing.T) {
		testRow := &Row{}
		testRow.Flags = []byte{0, 0, 0, 0, 0, 0, 0, 0}
		var testBytes = []byte{0, 0, 0, 0, 0, 0, 0, 0, 1}
		testRow.Data = append(testRow.Data, testBytes)
		testRow.reworkGeometry(0)
		if len(testRow.Data) != 1 {
			t.Errorf("Geometry data was not expected length of 1 but recieved: %v.\n", len(testRow.Data))
		}
		if testRow.Data[0].([]byte)[0] != 1 {
			t.Errorf("The first byte of geomtery was not expected value.\n")
		}
	})
	t.Run("Strip GeoPackage Binary Header with XY envelope.", func(t *testing.T) {
		testRow := &Row{}
		testRow.Flags = []byte{0, 0, 0, 0, 0, 0, 1, 0}
		var testBytes = []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}
		testRow.Data = append(testRow.Data, testBytes)
		testRow.reworkGeometry(0)
		if len(testRow.Data) != 1 {
			t.Errorf("Geometry data was not expected length of 1 but recieved: %v.\n", len(testRow.Data))
		}
		if testRow.Data[0].([]byte)[0] != 1 {
			t.Errorf("The first byte of geomtery was not expected value.\n")
		}
	})
	t.Run("Strip GeoPackage Binary Header with XYZ envelope.", func(t *testing.T) {
		testRow := &Row{}
		testRow.Flags = []byte{0, 0, 0, 0, 0, 1, 1, 0}
		var testBytes = []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}
		testRow.Data = append(testRow.Data, testBytes)
		testRow.reworkGeometry(0)
		if len(testRow.Data) != 1 {
			t.Errorf("Geometry data was not expected length of 1 but recieved: %v.\n", len(testRow.Data))
		}
		if testRow.Data[0].([]byte)[0] != 1 {
			t.Errorf("The first byte of geomtery was not expected value.\n")
		}
	})
	t.Run("Strip GeoPackage Binary Header with XYM envelope.", func(t *testing.T) {
		testRow := &Row{}
		testRow.Flags = []byte{0, 0, 0, 0, 1, 0, 0, 0}
		var testBytes = []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}
		testRow.Data = append(testRow.Data, testBytes)
		testRow.reworkGeometry(0)
		if len(testRow.Data) != 1 {
			t.Errorf("Geometry data was not expected length of 1 but recieved: %v.\n", len(testRow.Data))
		}
		if testRow.Data[0].([]byte)[0] != 1 {
			t.Errorf("The first byte of geomtery was not expected value.\n")
		}
	})
	t.Run("Strip GeoPackage Binary Header with XYMZ envelope.", func(t *testing.T) {
		testRow := &Row{}
		testRow.Flags = []byte{0, 0, 0, 0, 0, 1, 0, 0}
		var testBytes = []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}
		testRow.Data = append(testRow.Data, testBytes)
		testRow.reworkGeometry(0)
		if len(testRow.Data) != 1 {
			t.Errorf("Geometry data was not expected length of 1 but recieved: %v.\n", len(testRow.Data))
		}
		if testRow.Data[0].([]byte)[0] != 1 {
			t.Errorf("The first byte of geomtery was not expected value.\n")
		}
	})
}
