// Copyright 2020 Roma Hicks

// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.

// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

package geopackage

import (
	"bytes"
	"database/sql"
	"errors"
	"io"
	"log"
	"os"
	"path/filepath"
	"time"

	// Init import required by the SQLITE3 driver per dev instructions.
	_ "github.com/mattn/go-sqlite3"
)

// Constants for identifying GeoPackages by file header.
var headerString = []byte("SQLite format 3\000")
var applicationID = []byte("GPKG")
var userVersion = []byte{00, 00, 39, 216} // GeoPackage version number in bytes.

// GeoPackage is a structure representing the GeoPackage database and its API.
// It is the handle used by foreign packages to reference and manipulate the GeoPackage.
type GeoPackage struct {
	Path       string                      // Path to the GeoPackage.
	DB         *sql.DB                     // Handle to SQLite3 database for custom SQL statements.
	RefSystems map[int64]*SpatialRefSystem // List of SRS/CRS found in the GeoPackage.
	Content    []*Content                  // Actual layers found in the content table of the GeoPackage.
}

// SpatialRefSystem is the spatial reference system data found in gpkg_spatial_ref_sys.
type SpatialRefSystem struct {
	Name         string
	ID           int64
	Organisation string
	OrgSRSID     int64
	Definition   string // WKT Definition
	Description  string
	name         sql.NullString
	id           sql.NullInt64
	organisation sql.NullString
	orgsrsid     sql.NullInt64
	definition   sql.NullString
	description  sql.NullString
}

// Content is a structural reprsentation of data found in gpkg_contents.
type Content struct {
	TableName   string
	DataType    string
	Identifier  string
	Description string
	LastChange  time.Time
	MinX        float64
	MinY        float64
	MaxX        float64
	MaxY        float64
	SRSID       int64
	Data        *LayerData // Structure where actual layer rows are stored.
	tablename   sql.NullString
	datatype    sql.NullString
	identifier  sql.NullString
	description sql.NullString
	lastchange  sql.NullString
	minx        sql.NullFloat64
	miny        sql.NullFloat64
	maxx        sql.NullFloat64
	maxy        sql.NullFloat64
	srsid       sql.NullInt64
}

// OpenGeoPackage opens a GeoPackage at the specified URI.
// Opening a GeoPackage returns a pointer handle to the database for queries and writes.
//
// If the database fails GeoPackage validity checks, the handle to the database is still
// returned if possible, but an error will be thrown. Any subsquent, built-in api calls
// will have undefined behaviour.
func OpenGeoPackage(path string) (*GeoPackage, error) {
	cleanedPath, err := filepath.Abs(path)
	if err != nil {
		return nil, err
	}
	gp := GeoPackage{Path: cleanedPath}

	db, err := sql.Open("sqlite3", gp.Path)
	gp.DB = db
	if err != nil {
		return &gp, err
	}

	log.Printf("GEOPACKAGE: Opened GeoPackage - %v.\n", gp.Path)

	err = gp.checkHeader()
	if err != nil {
		return &gp, err
	}

	err = gp.checkForRequiredTables()
	if err != nil {
		return &gp, err
	}

	err = gp.loadSRS()
	if err != nil {
		return &gp, err
	}

	return &gp, nil
}

// checkHeader examines the loaded GeoPackage's file header.
// Examines the header to ensure it meets the GeoPackage standard and
// throws an error if the GeoPackage is malformed or doesn't appear to
// meet the standard.
func (gp *GeoPackage) checkHeader() error {
	var err error
	var headerBuf = make([]byte, 100)
	// Open the GeoPackage as a binary file for examination.
	f, err := os.Open(gp.Path)
	if err != nil {
		return err
	}
	defer f.Close()
	// Read the database header.
	_, err = io.ReadFull(f, headerBuf)
	if err != nil {
		return err
	}
	// Check the headerString is a SQLite3 database.
	if bytes.Equal(headerBuf[0:16], headerString) != true {
		return errors.New("file does not appear to be a SQLite3 database")
	}
	// Check the applicationID.
	if bytes.Equal(headerBuf[68:72], applicationID) != true {
		return errors.New("file does not appear to be a GeoPackage")
	}
	// Check GeoPackage version. Only support 1.2 at the moment.
	if !bytes.Equal(headerBuf[60:64], userVersion) {
		return errors.New("file is not a supported GeoPackage version")
	}
	return nil
}

// checkForRequiredTables examines the GeoPackage for the required tables.
// Queries the GeoPackage for the required tables that are expected by a
// well-formed database.  Throws an error if the tables are not found.
func (gp *GeoPackage) checkForRequiredTables() error {
	r1, err := gp.DB.Query("select name from sqlite_master where name=\"gpkg_contents\";")
	defer r1.Close()
	if err != nil {
		return err
	}
	if r1.Next() != true {
		err = r1.Err()
		if err != nil {
			return err
		}
		return errors.New("GeoPackage is missing required table: gpkg_contents")
	}
	r2, err := gp.DB.Query("select name from sqlite_master where name=\"gpkg_spatial_ref_sys\";")
	defer r2.Close()
	if err != nil {
		return err
	}
	if r2.Next() != true {
		err = r2.Err()
		if err != nil {
			return err
		}
		return errors.New("GeoPackage is missing required table: gpkg_spatial_ref_sys")
	}
	return nil
}

// loadSRS loads spatial reference systems from gpkg_spatial_ref_sys.
func (gp *GeoPackage) loadSRS() error {
	gp.RefSystems = make(map[int64]*SpatialRefSystem)
	rows, err := gp.DB.Query("select srs_name, srs_id, organization, organization_coordsys_id, definition, description from gpkg_spatial_ref_sys;")
	defer rows.Close()
	if err != nil {
		return err
	}

	for {
		srs := &SpatialRefSystem{}

		if rows.Next() != true {
			err = rows.Err()
			if err != nil {
				return err
			}
			return nil
		}
		err = rows.Scan(
			&srs.name,
			&srs.id,
			&srs.organisation,
			&srs.orgsrsid,
			&srs.definition,
			&srs.description)
		if err != nil {
			return err
		}

		// Check if scanned fields are valid NullTypes and populate public fields if valid.
		if srs.name.Valid {
			srs.Name = srs.name.String
		} else {
			return errors.New("required field was not valid")
		}
		if srs.id.Valid {
			srs.ID = srs.id.Int64
		} else {
			return errors.New("required field was not valid")
		}
		if srs.organisation.Valid {
			srs.Organisation = srs.organisation.String
		} else {
			return errors.New("required field was not valid")
		}
		if srs.orgsrsid.Valid {
			srs.OrgSRSID = srs.orgsrsid.Int64
		} else {
			return errors.New("required field was not valid")
		}
		if srs.definition.Valid {
			srs.Definition = srs.definition.String
		} else {
			return errors.New("required field was not valid")
		}
		if srs.description.Valid {
			srs.Description = srs.description.String
		}

		gp.RefSystems[srs.ID] = srs
	}
}

// LoadContent processes the gpkg_content table and load contents into RAM.
func (gp *GeoPackage) LoadContent() error {
	var err error
	gp.Content, err = gp.listContent()
	if err != nil {
		return err
	}

	var loadError error
	for i := range gp.Content {
		gp.Content[i].Data, err = gp.loadData(gp.Content[i].TableName)
		if err != nil {
			loadError = err
		}
	}
	if loadError != nil {
		return err
	}

	return nil
}

// ListContent loads all the tables found in the content table of the GeoPackage.
func (gp *GeoPackage) listContent() ([]*Content, error) {
	var contents []*Content
	r, err := gp.DB.Query("select table_name, data_type, identifier, description, last_change, min_x, min_y, max_x, max_y, srs_id from gpkg_contents where data_type=\"features\";")
	defer r.Close()
	if err != nil {
		return nil, err
	}

	for {
		c := &Content{}
		if r.Next() != true {
			err = r.Err()
			if err != nil {
				return nil, err
			}
			return contents, nil
		}
		err = r.Scan(
			&c.tablename,
			&c.datatype,
			&c.identifier,
			&c.description,
			&c.lastchange,
			&c.minx,
			&c.miny,
			&c.maxx,
			&c.maxy,
			&c.srsid)
		if err != nil {
			return nil, err
		}

		// Check if scanned fields are valid and populate if valid.
		if c.tablename.Valid {
			c.TableName = c.tablename.String
		} else {
			return nil, errors.New("required field was not valid")
		}
		if c.datatype.Valid {
			c.DataType = c.datatype.String
		} else {
			return nil, errors.New("required field was not valid")
		}
		if c.identifier.Valid {
			c.Identifier = c.identifier.String
		}
		if c.description.Valid {
			c.Description = c.description.String
		}
		if c.lastchange.Valid {
			c.LastChange, err = time.Parse(time.RFC3339, c.lastchange.String)
			if err != nil {
				return nil, err
			}
		} else {
			return nil, errors.New("required field was not valid")
		}
		if c.minx.Valid {
			c.MinX = c.minx.Float64
		}
		if c.miny.Valid {
			c.MinY = c.miny.Float64
		}
		if c.maxx.Valid {
			c.MaxX = c.maxx.Float64
		}
		if c.maxy.Valid {
			c.MaxY = c.maxy.Float64
		}
		if c.srsid.Valid {
			c.SRSID = c.srsid.Int64
		}

		contents = append(contents, c)
		log.Printf("GEOPACKAGE: Layer loaded - %v\n", c.TableName)
	}
}
