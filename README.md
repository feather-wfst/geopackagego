# GeoPackageGo

*This project is of alpha quality. Not all functionality is present and there is no optimization.*

GeoPackageGo is a Go library for GeoPackage management. GeoPackages are data containers for geospatial data and are built upon SQLite. GeoPackage automates some of the extraction and processing of GeoPackage containers but still allows the raw access to the SQLite database.
